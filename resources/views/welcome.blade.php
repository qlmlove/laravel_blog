﻿﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>柠娇黎</title>
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-grid.min.css"/>

    <!--播放器-->
    <link rel="stylesheet" href="/css/player.css">
<link rel="stylesheet" href="/css/guide-page.css">
    <link href="/css/location.css" type="text/css" rel="stylesheet">

</head>

<script>
    function closePage() {
        // window.location = "//www.baidu.com"; //将当前窗口跳转置空白页
        // window.close(); //关闭当前窗口(防抽)
    }

    function ck() {
        console.profile();
        console.profileEnd();
        //我们判断一下profiles里面有没有东西，如果有，肯定有人按F12了，没错！！
        if (console.clear) {
            console.clear()
        }
        ;
        if (typeof console.profiles == "object") {
            return console.profiles.length > 0;
        }
    }

    function hehe() {
        if ((window.console && (console.firebug || console.table && /firebug/i.test(console.table()))) || (typeof opera == 'object' && typeof opera.postError == 'function' && console.profile.length > 0)) {
            closePage();
        }
        if (typeof console.profiles == "object" && console.profiles.length > 0) {
            closePage();
        }
    }

    hehe();
    window.onresize = function () {
        if ((window.outerHeight - window.innerHeight) > 200)
//判断当前窗口内页高度和窗口高度，如果差值大于200，那么呵呵
            closePage();
    }</script>
<script type="text/javascript">
    document.onkeydown = function (event) {
        if ((event.ctrlKey) && (event.keyCode == 115 || event.keyCode == 83)) {
            event.returnValue = false;
            return;
        }
    }
</script>

<body class="body--ready" data-pinterest-extension-installed="cr1.39.1" oncontextmenu=self.event.returnValue=false
      onselectstart="return false">

<canvas class="canvas" width="100%" height="100%"></canvas>
<div id="willerce">
    <div>
        <img src="/images/1.jpg" id="logo" title="EB89"/>
        <h1>愛文迪客居</h1>
        <a>柠娇黎</a>
    </div>
    <div class="menu">
        <a style=" padding: 0px 20px; " class="btn btn-lg red" href="/home" target="_blank">首页</a>
        <a style=" padding: 0px 20px; " class="btn btn-lg green" href="#" target="_blank">文章</a>
        <a style=" padding: 0px 20px; " class="btn btn-lg bai" href="#" target="_blank">留言</a>
        <a style=" padding: 0px 20px; " class="btn btn-lg ju" href="javascript:void(0);">关于</a>
    </div>
</div>
<span class="copyright">banquan</span>
<script src="/js/action.js"></script>
<!--播放器 -->

<div id="QPlayer">
    <div id="pContent">
        <div id="player">
            <span class="cover"></span>
            <div class="ctrl">
                <div class="musicTag marquee">
                    <strong>Title</strong>
                    <span> - </span>
                    <span class="artist">Artist</span>
                </div>
                <div class="progress">
                    <div class="timer left">0:00</div>
                    <div class="contr">
                        <div class="rewind icon"></div>
                        <div class="playback icon"></div>
                        <div class="fastforward icon"></div>
                    </div>
                    <div class="right">
                        <div class="liebiao icon"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ssBtn">
            <div class="adf"></div>
        </div>
    </div>
    <ol id="playlist"></ol>
</div>

<script src="/js/jquery.min.js"></script>
<script src="/js/jquery.marquee.min.js"></script>

<script>
    var playlist = [
        {
            title: "wiat for love",
            artist: "elliott yamin",
            mp3: "https://m10.music.126.net/20180822103719/43440bc98cbfcb1cc7e195a050037c83/ymusic/5c0c/c84b/71cb/ae413fed229b3e2fb4f432a6b822e73d.mp3",
            cover: "https://p4.music.126.net/NpR4nUoJXsQmWunZWCz4OQ==/898300999944140.jpg",
        },
        {
            title: "优美的小调(钢琴曲)",
            artist: "张宇桦",
            mp3: "//music.163.com/song/media/outer/url?id=28160230",
            cover: "//p3.music.126.net/CWeAKWr06dC8pc0rajYN_w==/109951162811772268.jpg?param=106x106",
        },
        {
            title: "风中的蒲公英(钢琴曲)",
            artist: "张宇桦",
            mp3: "//music.163.com/song/media/outer/url?id=28160231",
            cover: "//p4.music.126.net/CWeAKWr06dC8pc0rajYN_w==/109951162811772268.jpg?param=106x106",
        },
        {
            title: "Sun",
            artist: "Steerner",
            mp3: "//music.163.com/song/media/outer/url?id=505666217",
            cover: "//p3.music.126.net/xEuUgUnosjgJxpKANkJX3g==/1401877332796018.jpg?param=106x106",
        },

    ];
    var isRotate = true;
    var autoplay = true;
</script>
<script src="/js/player.js"></script>
<script>

    function bgChange() {
        var lis = $('.lib');
        for (var i = 0; i < lis.length; i += 2)
            lis[i].style.background = 'rgba(246, 246, 246, 0.5)';
    }

    window.onload = bgChange;
</script>
{{--<div>统计代码</div>--}}
</body>
<div></div>
</html>
